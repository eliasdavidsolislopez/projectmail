import string
from flask import Flask, render_template, request
import hashlib
import controlador
from datetime import datetime
import envioemail
from flask_socketio import SocketIO, join_room, leave_room
import socketio
from socket import socket


app = Flask(__name__)

app.config['SECRET_KEY']="abc123"
app.config['DEBUG']=True
socketio=SocketIO(app, cors_allowed_origins="*")

@app.route("/")
def inicio():
    return render_template("login.html")

@app.route("/validarUsuario",methods=["GET", "POST"])
def validarUsuario():
    if request.method=="POST":
        usu=request.form["txtusuario"]
        passw=request.form["txtpass"]

        passw2=passw.encode()
        passw2=hashlib.sha384(passw2).hexdigest()

        respuesta=controlador.validar_usuario(usu, passw2)

        if len(respuesta)==0:
            mensaje="¡EEROR DE AUTENTICACIÓN!  Por favor verifique su usuario (correo) y contraseña"
            return render_template("informacion.html", infos=mensaje)
        else:

            return render_template("principal.html")

@app.route("/registrarUsuario",methods=["GET", "POST"])
def registrarUsuario():
    if request.method=="POST":
        name=request.form["txtnombre"]
        email=request.form["txtusuario2registro"]
        passw=request.form["txtpassregistro"]

        passw2=passw.encode()
        passw2=hashlib.sha384(passw2).hexdigest()

        codigo=datetime.now()
        codigo2=str(codigo)
        codigo2=codigo2.replace("-", "")
        codigo2=codigo2.replace(" ", "")
        codigo2=codigo2.replace(":", "")
        codigo2=codigo2.replace(".", "")
        print(codigo2)

        envioemail.enviar(email,codigo2)

        respuesta=controlador.registrar_usuario(name,email,passw2, codigo2)

        mensaje="El usuario " +name+ ", se ha registrado satisfactoriamente"
        return render_template("informacion.html", infos=mensaje)

@app.route("/activarUsuario",methods=["GET", "POST"])
def activarUsuario():
    if request.method=="POST":
        codigo=request.form["txtcodigo"]

        respuesta=controlador.activar_usuario(codigo)

        if len(respuesta)==0:
            mensaje="El codigo de activación ingresado es erroneo, verifíquelo"
        else:
            mensaje="Usuario activado existosamente"
        return render_template("informacion.html", infos=mensaje)



@app.route("/chat")
def chat():
    return render_template("mail.html")

@socketio.on('connected')
def connected(data):
    print(data)

#primer evento 
@socketio.on("message")
def get_message(json, methods="POST"):
    print("mensaje"+ str(json))
    socketio.emit("response",json)


if __name__=="__main__":
    socketio.run(app)
    











